CC = gcc
AR = ar

OUT_LIB_NAME = libcoopscheduler.a
OUT_DEMO_NAME = cooptest
CFLAGS = -fPIC -O3 -s -Wall -c -fpermissive

OBJ_DIR = ./obj
OUT_DIR = ./lib

all: dirs $(OUT_LIB_NAME) $(OUT_DEMO_NAME) test clean

$(OUT_LIB_NAME): $(OBJ_DIR)/sched.o
	$(AR) -r $(OUT_DIR)/$@ $^

$(OBJ_DIR)/sched.o: sched.c
	$(CC) -c $(CFLAGS) -o $@ $^

$(OUT_DEMO_NAME): main.c
	$(CC) -L$(OUT_DIR) -lcoopscheduler -O3 -s -o $(OUT_DIR)/$@ $^

dirs:
	mkdir $(OBJ_DIR) $(OUT_DIR)

test:
	$(OUT_DIR)/$(OUT_DEMO_NAME)

clean:
	rm -r $(OBJ_DIR) $(OUT_DIR)

.PHONY: all dirs test clean
