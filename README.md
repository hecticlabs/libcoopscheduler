
# libcoopscheduler
Cooperative scheduler multi-tasking oriented at the Raspherry Pi Pico.


Inspired by [Stephen Brennan's Implementing simple cooperative threads in C](https://brennan.io/2020/05/24/userspace-cooperative-multitasking/) article, used their scheduling logic and stack switching.
