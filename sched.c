#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>

#include "sched.h"

struct sched_task {
  enum {
    TASK_CREATED,
    TASK_RUNNING,
    TASK_WAITING,
  } status;

  int tid;

  jmp_buf buf;

  void (*func)(void*);
  void *arg;

  struct sched_task *next, *prev;

  void *stack_bot, *stack_top;
  int stack_len;
};

enum {
  SCHED_INIT=0,
  SCHED_SCHEDULE,
  SCHED_EXIT_TASK,
};

struct sched_internal {
  jmp_buf buf;

  struct sched_task *current, *head, *tail;
} internal;

void sched_init(void) {
  internal.current = NULL;
  internal.head = NULL;
  internal.tail = NULL;
}

static inline void linked_list_remove_task(struct sched_task *task) {
  if (task->prev == NULL && task->next == NULL) {
    task->prev = NULL;
    task->next = NULL;
    internal.head = NULL;
    internal.tail = NULL;
    return;
  }

  if (task->prev == NULL) {
    internal.head = task->next;
  } else {
    task->prev->next = task->next;
  }
  if (task->next == NULL) {
    internal.tail = task->prev;
  } else {
    task->next->prev = task->prev;
  }
  task->prev = NULL;
  task->next = NULL;
}

static inline void linked_list_append_task(struct sched_task *task) {
  if (internal.head == NULL) {
    internal.head = task;
    internal.tail = task;
  } else {
    internal.tail->next = task;
    task->prev = internal.tail;
    internal.tail = task;
  }
}

void sched_push_task(void (*func)(void*), void *arg) {
  static int tid = 1;
  struct sched_task *task = malloc(sizeof(*task));
  task->status = TASK_CREATED;
  task->func = func;
  task->arg = arg;
  task->tid = tid++;
  task->stack_len = STACK_SIZE;
  task->stack_bot = malloc(task->stack_len);
  task->stack_top = task->stack_bot + task->stack_len;
  task->prev = NULL;
  task->next = NULL;

  linked_list_append_task(task);
}

void sched_exit_task(void) {
  struct sched_task *task = internal.current;
  linked_list_remove_task(task);

  longjmp(internal.buf, SCHED_EXIT_TASK);
}

static struct sched_task *choose_task(void) {
  struct sched_task *task = internal.head;
  if (task == NULL) {
    return NULL;
  }

  do {
    if (task->status == TASK_RUNNING || task->status == TASK_CREATED) {
      linked_list_remove_task(task);
      linked_list_append_task(task);
      return task;
    }
  } while ((task = task->next) != NULL);

  return NULL;
}

static void schedule() {
  struct sched_task *next = choose_task();

  if (next == NULL) {
    return;
  }

  internal.current = next;
  if (next->status == TASK_CREATED) {
    register void *top = next->stack_top;

#if defined(__x86_64__)
    asm volatile(
      "mov %[rs], %%rsp \n"
      : [ rs ] "+r" (top) ::
    );
#elif defined(__i386__)
    asm volatile(
      "mov %0, %%esp \n"
      :
      : "r" (top)
    );
#elif defined(__aarch64__) || defined(__arm__)
    asm volatile(
      "mov sp, %[rs]\n"
      : [rs] "r" (top)
      : "sp"
    );
#else
  #error "Architecture not implemented."
#endif

    next->status = TASK_RUNNING;
    next->func(next->arg);

    sched_exit_task();
  } else {
    longjmp(next->buf, 1);
  }
}

void sched_yield(void) {
  if (setjmp(internal.current->buf)) {
    return;
  } else {
    longjmp(internal.buf, SCHED_SCHEDULE);
  }
}

static void free_current_task(void) {
  struct sched_task *task = internal.current;
  internal.current = NULL;
  free(task->stack_bot);
  free(task);
}

void sched_run(void) {
  switch (setjmp(internal.buf)) {
  case SCHED_EXIT_TASK:
    free_current_task();
  case SCHED_INIT:
  case SCHED_SCHEDULE:
    schedule();
    return;
  default:
    // memory must have been corrupted, panic
    *((void*)0);
    return;
  }
}


