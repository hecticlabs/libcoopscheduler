#ifndef _LIBCOOPSCHEDULER_SCHED_H
#define _LIBCOOPSCHEDULER_SCHED_H

#define STACK_SIZE (8 * 1024)

void sched_init(void);
void sched_push_task(void (*func)(void*), void *arg);
void sched_run(void);
void sched_exit_task(void);
void sched_yield(void);

#endif
