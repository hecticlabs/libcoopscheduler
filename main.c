#include <stdio.h>
#include <stdlib.h>

#include "sched.h"

struct test_args {
  char *name;
  int iters;
};

void test(void *arg) {
  int i;
  struct test_args *ta = (struct test_args *)arg;
  for (i = 0; i < ta->iters; i++) {
    printf("task %s: %d\n", ta->name, i);
    sched_yield();
  }
  printf("task %s: exit\n", ta->name);
  free(ta);
}

void create_test_task(char *name, int iters) {
  struct test_args *ta = malloc(sizeof(*ta));
  ta->name = name;
  ta->iters = iters;
  sched_push_task(test, ta);
}

int main(int argc, char **argv) {
  sched_init();
  create_test_task("first", 5);
  create_test_task("second", 2);
  sched_run();
  printf("Finished running all tasks!\n");
  return EXIT_SUCCESS;
}
